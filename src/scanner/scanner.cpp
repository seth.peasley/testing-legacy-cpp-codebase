//  *************************************************************
//  *                                                           *
//  *   S C A N N E R                                           *
//  *                                                           *
//  *   Scan the text input file.                               *
//  *                                                           *
//  *   CLASSES: TTextScanner                                   *
//  *                                                           *
//  *   FILE:    prog3-2/scanner.cpp                            *
//  *                                                           *
//  *   MODULE:  Scanner                                        *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include "scanner.h"

CharacterCode charCodeMap[128];    // maps a character to its code

//--------------------------------------------------------------
//  Construct a scanner by constructing the text input file buffer 
//  and initializing the character code map.
//
//      pBuffer : ptr to text input buffer to scan
//--------------------------------------------------------------
TTextScanner::TTextScanner(TTextInBuffer *pBuffer) : pTextInBuffer(pBuffer)
{
    int i;

    //--Initialize the character code map.
    for (i =   0; i <  128; ++i) charCodeMap[i] = CharacterCode::ccError;
    for (i = 'a'; i <= 'z'; ++i) charCodeMap[i] = CharacterCode::ccLetter;
    for (i = 'A'; i <= 'Z'; ++i) charCodeMap[i] = CharacterCode::ccLetter;
    for (i = '0'; i <= '9'; ++i) charCodeMap[i] = CharacterCode::ccDigit;
    charCodeMap['+' ] = charCodeMap['-' ] = CharacterCode::ccSpecial;
    charCodeMap['*' ] = charCodeMap['/' ] = CharacterCode::ccSpecial;
    charCodeMap['=' ] = charCodeMap['^' ] = CharacterCode::ccSpecial;
    charCodeMap['.' ] = charCodeMap[',' ] = CharacterCode::ccSpecial;
    charCodeMap['<' ] = charCodeMap['>' ] = CharacterCode::ccSpecial;
    charCodeMap['(' ] = charCodeMap[')' ] = CharacterCode::ccSpecial;
    charCodeMap['[' ] = charCodeMap[']' ] = CharacterCode::ccSpecial;
    charCodeMap['{' ] = charCodeMap['}' ] = CharacterCode::ccSpecial;
    charCodeMap[':' ] = charCodeMap[';' ] = CharacterCode::ccSpecial;
    charCodeMap[' ' ] = charCodeMap['\t'] = CharacterCode::ccWhiteSpace;
    charCodeMap['\n'] = charCodeMap['\0'] = CharacterCode::ccWhiteSpace;
    charCodeMap['\'']    =CharacterCode:: ccQuote;
    charCodeMap[eofChar] = CharacterCode::ccEndOfFile;
}

//--------------------------------------------------------------
//  Repeatedly fetch characters from the text input as long as they're
//  whitespace. Each comment is a whitespace character.
//--------------------------------------------------------------
void TTextScanner::SkipWhiteSpace(void)
{
    char ch = pTextInBuffer->Char();

    do 
    {
        if (charCodeMap[ch] == CharacterCode::ccWhiteSpace) 
        {
            //--Saw a whitespace character:  fetch the next character.
            ch = pTextInBuffer->GetChar();
        }
        else if (ch == '{') 
        {
            //--Skip over a comment, then fetch the next character.
            do 
            {
                ch = pTextInBuffer->GetChar();
            } while ((ch != '}') && (ch != eofChar));

            if (ch != eofChar) ch = pTextInBuffer->GetChar();
            else               Error(errUnexpectedEndOfFile);
        }
    } while ((charCodeMap[ch] == CharacterCode::ccWhiteSpace) || (ch == '{'));
}


//--------------------------------------------------------------
//  Extract the next token from the text input, based on the current character.
//
//  Return: pointer to the extracted token
//--------------------------------------------------------------
TToken *TTextScanner::Get(void)
{
    TToken *pToken;  // ptr to token to return

    SkipWhiteSpace();

    //--Determine the token class, based on the current character.
    switch (charCodeMap[pTextInBuffer->Char()]) 
    {
        case CharacterCode::ccLetter:
            pToken = &wordToken;
            break;
        case CharacterCode::ccDigit:
            pToken = &numberToken;
            break;
        case CharacterCode::ccQuote:
            pToken = &stringToken;
            break;
        case CharacterCode::ccSpecial:
            pToken = &specialToken; 
            break;
        case CharacterCode::ccEndOfFile:
            pToken = &eofToken;
            break;
        default:          
            pToken = &errorToken;
            break;
    }

    //--Extract a token of that class, and return a pointer to it.
    pToken->Get(*pTextInBuffer);
    return pToken;
}
