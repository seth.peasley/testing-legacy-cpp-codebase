// Main compiler code starting point

#include <iostream>
#include "commonpascal.h"
#include "error.h"
#include "buffer.h"
#include "symtab.h"
#include "parser.h"
#include "backend.h"
#include "codegen.h"

//--------------------------------------------------------------
//  main
//--------------------------------------------------------------
int main(int argc, char *argv[])
{

    std::cout << "Hello, world!" << std::endl;

  
    extern int execFlag;

    //--Check the command line arguments.
    if (argc != 3) 
    {
        std::cerr << "Usage: compile <source file> <asssembly file>" << std::endl;
        AbortTranslation(abortInvalidCommandLineArgs);
    }

    execFlag = false;

    //--Create the parser for the source file,
    //--and then parse the file.
    TParser     *pParser    = new TParser(new TSourceBuffer(argv[1]));
    TSymtabNode *pProgramId = pParser->Parse();
    delete pParser;

    //--If there were no syntax errors, convert the symbol tables,
    //--and create and invoke the backend code generator.
    if (errorCount == 0) {
    vpSymtabs = new TSymtab *[cntSymtabs];
    for (TSymtab *pSt = pSymtabList; pSt; pSt = pSt->Next()) {
        pSt->Convert(vpSymtabs);
    }

    TBackend *pBackend = new TCodeGenerator(argv[2]);
    pBackend->Go(pProgramId);

    delete[] vpSymtabs;
    delete   pBackend;
    }
    

   return 0;
}

// // // // Print yes
// // // void temp()
// // // {
// // //     std::cout << "yes." << std::endl;
// // // }


//fig 14-21
//  *************************************************************
//  *                                                           *
//  *   Program 14-1: Compiler                                  *
//  *                                                           *
//  *   Compile a Pascal program into assembly code.            *
//  *                                                           *
//  *   FILE:   prog14-1/compile.cpp                            *
//  *                                                           *
//  *   USAGE:  compile <source file> <assembly file>           *
//  *                                                           *
//  *               <source file>    name of the source file    *
//  *               <assembly file>  name of the assembly       *
//  *                                  language output file     *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************