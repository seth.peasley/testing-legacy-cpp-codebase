//fig 12-9
//  *************************************************************
//  *                                                           *
//  *   E M I T   S O U R C E   L I N E S                       *
//  *                                                           *
//  *   Emit source lines as comments in the assembly listing.  *
//  *                                                           *
//  *   CLASSES: TCodeGenerator                                 *
//  *                                                           *
//  *   FILE:    prog13-1/emitsrc.cpp                           *
//  *                                                           *
//  *   MODULE:  Code generator                                 *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include <stdio.h>
#include "commonpascal.h"
#include "buffer.h"
#include "symtab.h"
#include "codegen.h"

//--------------------------------------------------------------
//  Start a new comment with a line number.
//
//      pString : ptr to the string to append
//--------------------------------------------------------------
void TCodeGenerator::StartComment(int n)
{
    Reset();
    sprintf(AsmText(), "; {%d} ", n);
    Advance();
}

//--------------------------------------------------------------
//  Append a string to the assembly comment if it fits.  If not, 
//  emit the current comemnt and append the string to the next comment.
//
//      pString : ptr to the string to append
//--------------------------------------------------------------
void TCodeGenerator::PutComment(const char *pString)
{
    int length = strlen(pString);

    //--Start a new comment if the current one is full.
    if (!pAsmBuffer->Fit(length)) 
    {
        PutLine();
        StartComment();
    }

    strcpy(AsmText(), pString);
    Advance();
}


//              ******************
//              *                *
//              *  Declarations  *
//              *                *
//              ******************

//--------------------------------------------------------------
//  Emit the program header as a comment.
//
//      pProgramId : ptr to the program id's symbol table node
//--------------------------------------------------------------
void TCodeGenerator::EmitProgramHeaderComment
                    (const TSymtabNode *pProgramId)
{
    PutLine();
    StartComment("PROGRAM ");
    PutComment(pProgramId->String());  // program name

    //--Emit the program's parameter list.
    TSymtabNode *pParmId = pProgramId->defn.routine.locals.pParmIds;
    if (pParmId) {
    PutComment(" (");

    //--Loop to emit each parameter.
    do {
        PutComment(pParmId->String());
        pParmId = pParmId->next;
        if (pParmId) PutComment(", ");
    } while (pParmId);

    PutComment(")");
    }

    PutLine();
}

//--------------------------------------------------------------
//  Emit a subroutine header as a comment.
//
//      pRoutineId : ptr to the subroutine id's symtab node
//--------------------------------------------------------------
void TCodeGenerator::EmitSubroutineHeaderComment
                    (const TSymtabNode *pRoutineId)
{
    PutLine();
    StartComment(pRoutineId->defn.how == dcProcedure ? "PROCEDURE "
                             : "FUNCTION " );
    //--Emit the procedure or function name
    //--followed by the formal parameter list.
    PutComment(pRoutineId->String());
    EmitSubroutineFormalsComment
            (pRoutineId->defn.routine.locals.pParmIds);

    //--Emit a function's return type.
    if (pRoutineId->defn.how == dcFunction) 
    {
        PutComment(" : ");
        PutComment(pRoutineId->pType->pTypeId->String());
    }

    PutLine();
}

//--------------------------------------------------------------
//  Emit a formal parameter list as a comment.
//
//      pParmId : ptr to the head of the formal parm id list
//--------------------------------------------------------------
void TCodeGenerator::EmitSubroutineFormalsComment(const TSymtabNode *pParmId)
{
    if (!pParmId) return;

    PutComment(" (");

    //--Loop to emit each sublist of parameters with
    //--common definition and type.
    do 
    {
        TDefnCode  commonDefn  = pParmId->defn.how;  // common defn
        TType     *pCommonType = pParmId->pType;     // common type
        int        doneFlag;  // true if sublist done, false if not

        if (commonDefn == dcVarParm) PutComment("VAR ");

        //--Loop to emit the parms in the sublist.
        do {
            PutComment(pParmId->String());

            pParmId  = pParmId->next;
            doneFlag = (!pParmId) || (commonDefn  != pParmId->defn.how)
                    || (pCommonType != pParmId->pType);
            if (!doneFlag) PutComment(", ");
        } while (!doneFlag);

        //--Print the sublist's common type.
        PutComment(" : ");
        PutComment(pCommonType->pTypeId->String());

        if (pParmId) PutComment("; ");
        } while (pParmId);

    PutComment(")");
}

//--------------------------------------------------------------
//  Emit variable declarations as comments.
//
//      pVarId : ptr to the head of the variable id list
//--------------------------------------------------------------
void TCodeGenerator::EmitVarDeclComment(const TSymtabNode *pVarId)
{
    TType *pCommonType;  // ptr to common type

    if (!pVarId) return;
    pCommonType = pVarId->pType;

    PutLine();
    StartComment("VAR");
    PutLine();
    StartComment();

    //--Loop to print sublists of variables with a common type.
    do 
    {
        PutComment(pVarId->String());
        pVarId = pVarId->next;

        if (pVarId && (pVarId->pType == pCommonType)) PutComment(", ");
        else 
        {
            //--End of sublist:  Print the common type and begin
            //--                 a new sublist.
            PutComment(" : ");
            EmitTypeSpecComment(pCommonType);
            PutLine();

            if (pVarId)
            {
                pCommonType = pVarId->pType;
                StartComment();
            }
        }
    } while (pVarId);
}

//--------------------------------------------------------------
//  Emit a type specification as a comment.
//
//      pType : ptr to the type object
//--------------------------------------------------------------
void TCodeGenerator::EmitTypeSpecComment(const TType *pType)
{
    //--If named type, emit the name, else emit "..."
    PutComment(pType->pTypeId ? pType->pTypeId->String() : "...");
}


//              ****************
//              *              *
//              *  Statements  *
//              *              *
//              ****************

//--------------------------------------------------------------
//  EmitStmtComment         Emit a statement as a comment.
//--------------------------------------------------------------

void TCodeGenerator::EmitStmtComment(void)
{
    SaveState();     // save icode state
    StartComment(currentLineNumber);

    switch (token) 
    {
        case TokenCode::Identifier:  EmitAsgnOrCallComment();    break;
        case TokenCode::REPEAT:      EmitREPEATComment();        break;
        case TokenCode::UNTIL:       EmitUNTILComment();         break;
        case TokenCode::WHILE:       EmitWHILEComment();         break;
        case TokenCode::IF:          EmitIFComment();            break;
        case TokenCode::FOR:         EmitFORComment();           break;
        case TokenCode::CASE:        EmitCASEComment();          break;
    }

    RestoreState();  // restore icode state
}

//--------------------------------------------------------------
//  Emit an assignment statement or a procedure call as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitAsgnOrCallComment(void)
{
    EmitIdComment();

    if (token == TokenCode::ColonEqual) 
    {
        PutComment(" := ");

        GetToken();
        EmitExprComment();
    }

    PutLine();
}

//--------------------------------------------------------------
//  Emit a REPEAT statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitREPEATComment(void)
{
    PutComment("REPEAT");
    PutLine();
}

//--------------------------------------------------------------
//  Emit a REPEAT statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitUNTILComment(void)
{
    PutComment("UNTIL ");

    GetToken();
    EmitExprComment();

    PutLine();
}

//--------------------------------------------------------------
//  Emit a WHILE statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitWHILEComment(void)
{
    PutComment("WHILE ");

    GetToken();
    GetLocationMarker();

    GetToken();
    EmitExprComment();

    PutComment(" DO");
    PutLine();
}

//--------------------------------------------------------------
//  Emit an IF statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitIFComment(void)
{
    PutComment("IF ");

    GetToken();
    GetLocationMarker();

    GetToken();
    EmitExprComment();

    PutLine();
}

//--------------------------------------------------------------
//  Emit a FOR statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitFORComment(void)
{
    PutComment("FOR ");

    GetToken();
    GetLocationMarker();

    GetToken();
    EmitIdComment();
    PutComment(" := ");

    GetToken();
    EmitExprComment();
    PutComment(token == TokenCode::TO ? " TO " : " DOWNTO ");

    GetToken();
    EmitExprComment();

    PutComment(" DO");
    PutLine();
}

//--------------------------------------------------------------
//  Emit a CASE statement as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitCASEComment(void)
{
    PutComment("CASE ");

    GetToken();
    GetLocationMarker();
    GetToken();
    GetLocationMarker();

    GetToken();
    EmitExprComment();

    PutComment(" OF ");
    PutLine();
}


//              ******************
//              *                *
//              *  Expresssions  *
//              *                *
//              ******************

//--------------------------------------------------------------
//  Emit an expression as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitExprComment(void)
{
    int doneFlag = false;  // true if done with expression, false if not

    //--Loop over the entire expression.
    do 
    {
        switch (token) 
        {
            case TokenCode::Identifier:  EmitIdComment();  break;

            case TokenCode::Number:  PutComment(pToken->String());  GetToken();
                    break;

            case TokenCode::String:  PutComment(pToken->String());  GetToken();
                    break;

            case TokenCode::Plus:    PutComment(" + ");    GetToken();  break;
            case TokenCode::Minus:   PutComment(" - ");    GetToken();  break;
            case TokenCode::Star:    PutComment("*");      GetToken();  break;
            case TokenCode::Slash:   PutComment("/");      GetToken();  break;
            case TokenCode::DIV:     PutComment(" DIV ");  GetToken();  break;
            case TokenCode::MOD:     PutComment(" MOD ");  GetToken();  break;
            case TokenCode::AND:     PutComment(" AND ");  GetToken();  break;
            case TokenCode::OR:      PutComment(" OR ");   GetToken();  break;
            case TokenCode::Equal:   PutComment(" = ");    GetToken();  break;
            case TokenCode::Ne:      PutComment(" <> ");   GetToken();  break;
            case TokenCode::Lt:      PutComment(" < ");    GetToken();  break;
            case TokenCode::Le:      PutComment(" <= ");   GetToken();  break;
            case TokenCode::Gt:      PutComment(" > ");    GetToken();  break;
            case TokenCode::Ge:      PutComment(" >= ");   GetToken();  break;
            case TokenCode::NOT:     PutComment("NOT ");   GetToken();  break;

            case TokenCode::LParen:
            PutComment("(");
            GetToken();
            EmitExprComment();
            PutComment(")");
            GetToken();
            break;

            default:
            doneFlag = true;
            break;
        }
    } while (!doneFlag);
}

//--------------------------------------------------------------
// Emit an identifier and its modifiers as a comment.
//  --Tokens that can start an identifier modifier.
//--------------------------------------------------------------
TokenCode tlIdModStart[] = {TokenCode::LBracket, TokenCode::LParen, TokenCode::Period, TokenCode::Dummy};

//--------------------------------------------------------------
// Emit an identifier and its modifiers as a comment.
//    --Tokens that can end an identifier modifier.
//--------------------------------------------------------------
TokenCode tlIdModEnd[]   = {TokenCode::RBracket, TokenCode::RParen, TokenCode::Dummy};

//--------------------------------------------------------------
// Emit an identifier and its modifiers as a comment.
//--------------------------------------------------------------
void TCodeGenerator::EmitIdComment(void)
{
    PutComment(pToken->String());
    GetToken();

    //--Loop to print any modifiers (subscripts, record fields,
    //--or actual parameter lists).
    while (TokenIn(token, tlIdModStart)) 
    {
        //--Record field.
        if (token == TokenCode::Period) 
        {
            PutComment(".");
            GetToken();
            EmitIdComment();
        }
        //--Subscripts or actual parameters.
        else 
        {
            //--( or [
            PutComment(token == TokenCode::LParen ? "(" : "[");
            GetToken();

            while (!TokenIn(token, tlIdModEnd)) 
            {
                EmitExprComment();

                //--Write and writeln field width and precision.
                while (token == TokenCode::Colon) 
                {
                    PutComment(":");
                    GetToken();
                    EmitExprComment();
                }

                if (token == TokenCode::Comma) 
                {
                    PutComment(", ");
                    GetToken();
                }
            }

            //--) or ]
            PutComment(token == TokenCode::RParen ? ")" : "]");
            GetToken();
        }
    }
}
//endfig
