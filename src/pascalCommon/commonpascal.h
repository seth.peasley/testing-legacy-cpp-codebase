//  *************************************************************
//  *                                                           *
//  *   C O M M O N   (Header)                                  *
//  *                                                           *
//  *   FILE:    prog8-1/commonpascal.h                               *
//  *                                                           *
//  *   MODULE:  Common                                         *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#ifndef commonpascal_h
#define commonpascal_h

#include "misc.h"
#include "symtab.h"
#include "intermediateCode.h"

extern int currentLineNumber;
extern int currentNestingLevel;

extern TSymtab   globalSymtab;
extern int       cntSymtabs;
extern TSymtab  *pSymtabList;
extern TSymtab **vpSymtabs;

//--------------------------------------------------------------
//  Token lists
//--------------------------------------------------------------

extern const TokenCode tlProcFuncStart[], tlProcFuncFollow[],
            tlHeaderFollow[];

extern const TokenCode tlProgProcIdFollow[],    tlFuncIdFollow[],
            tlActualVarParmFollow[], tlFormalParmsFollow[];

extern const TokenCode tlDeclarationStart[], tlDeclarationFollow[],
            tlIdentifierStart[],  tlIdentifierFollow[],
            tlSublistFollow[],    tlFieldDeclFollow[];

extern const TokenCode tlEnumConstStart[], tlEnumConstFollow[],
            tlSubrangeLimitFollow[];

extern const TokenCode tlIndexStart[], tlIndexFollow[],
            tlIndexListFollow[];

extern const TokenCode tlStatementStart[], tlStatementFollow[];
extern const TokenCode tlCaseLabelStart[];

extern const TokenCode tlExpressionStart[], tlExpressionFollow[];
extern const TokenCode tlRelOps[], tlUnaryOps[],
            tlAddOps[], tlMulOps[];

extern const TokenCode tlProgramEnd[];

extern const TokenCode tlColonEqual[];
extern const TokenCode tlDO[];
extern const TokenCode tlTHEN[];
extern const TokenCode tlTODOWNTO[];
extern const TokenCode tlOF[];
extern const TokenCode tlColon[];
extern const TokenCode tlEND[];

int TokenIn(TokenCode tc, const TokenCode *pList);

#endif
