//  *************************************************************
//  *                                                           *
//  *   C O M M O N                                             *
//  *                                                           *
//  *   FILE:    prog8-1/common.cpp                             *
//  *                                                           *
//  *   MODULE:  Common                                         *
//  *                                                           *
//  *   Data and routines common to the front and back ends.    *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include "commonpascal.h"

int currentNestingLevel = 0;
int currentLineNumber   = 0;

TSymtab   globalSymtab;        // the global symbol table
int       cntSymtabs  = 0;     // symbol table counter
TSymtab  *pSymtabList = NULL;  // ptr to head of symtab list
TSymtab **vpSymtabs;           // ptr to vector of symtab ptrs

//--------------------------------------------------------------
//  Token lists
//--------------------------------------------------------------


//--Tokens that can start a procedure or function definition.
extern const TokenCode tlProcFuncStart[] = {
    TokenCode::PROCEDURE, TokenCode::FUNCTION, TokenCode::Dummy
};

//--Tokens that can follow a procedure or function definition.
extern const TokenCode tlProcFuncFollow[] = {
    TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow a routine header.
extern const TokenCode tlHeaderFollow[] = {
    TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow a program or procedure id in a header.
extern const TokenCode tlProgProcIdFollow[] = {
    TokenCode::LParen, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow a function id in a header.
extern const TokenCode tlFuncIdFollow[] = {
    TokenCode::LParen, TokenCode::Colon, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow an actual variable parameter.
extern const TokenCode tlActualVarParmFollow[] = {
    TokenCode::Comma, TokenCode::RParen, TokenCode::Dummy
};

//--Tokens that can follow a formal parameter list.
extern const TokenCode tlFormalParmsFollow[] = {
    TokenCode::RParen, TokenCode::Semicolon, TokenCode::Dummy
};
//endfig

//--Tokens that can start a declaration.
extern const TokenCode tlDeclarationStart[] = {
   TokenCode::CONST, TokenCode::TYPE, TokenCode::VAR, TokenCode::PROCEDURE, TokenCode::FUNCTION, TokenCode::Dummy
};

//--Tokens that can follow a declaration.
extern const TokenCode tlDeclarationFollow[] = {
    TokenCode::Semicolon, TokenCode::Identifier, TokenCode::Dummy
};

//--Tokens that can start an identifier or field.
extern const TokenCode tlIdentifierStart[] = {
    TokenCode::Identifier, TokenCode::Dummy
};

//--Tokens that can follow an identifier or field.
extern const TokenCode tlIdentifierFollow[] = {
    TokenCode::Comma, TokenCode::Identifier, TokenCode::Colon, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow an identifier or field sublist.
extern const TokenCode tlSublistFollow[] = {
    TokenCode::Colon, TokenCode::Dummy
};

//--Tokens that can follow a field declaration.
extern const TokenCode tlFieldDeclFollow[] = {
    TokenCode::Semicolon, TokenCode::Identifier, TokenCode::END, TokenCode::Dummy
};

//--Tokens that can start an enumeration constant.
extern const TokenCode tlEnumConstStart[] = {
    TokenCode::Identifier, TokenCode::Dummy
};

//--Tokens that can follow an enumeration constant.
extern const TokenCode tlEnumConstFollow[] = {
    TokenCode::Comma, TokenCode::Identifier, TokenCode::RParen, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow a subrange limit.
extern const TokenCode tlSubrangeLimitFollow[] = {
    TokenCode::DotDot, TokenCode::Identifier, TokenCode::Plus, TokenCode::Minus, TokenCode::String,
    TokenCode::RBracket, TokenCode::Comma, TokenCode::Semicolon, TokenCode::OF, TokenCode::Dummy
};

//--Tokens that can start an index type.
extern const TokenCode tlIndexStart[] = {
    TokenCode::Identifier, TokenCode::Number, TokenCode::String, TokenCode::LParen, TokenCode::Plus, TokenCode::Minus,
    TokenCode::Dummy
};

//--Tokens that can follow an index type.
extern const TokenCode tlIndexFollow[] = {
    TokenCode::Comma, TokenCode::RBracket, TokenCode::OF, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can follow the index type list.
extern const TokenCode tlIndexListFollow[] = {
    TokenCode::OF, TokenCode::Identifier, TokenCode::LParen, TokenCode::ARRAY, TokenCode::RECORD,
    TokenCode::Plus, TokenCode::Minus, TokenCode::Number, TokenCode::String, TokenCode::Semicolon, TokenCode::Dummy
};

//--Tokens that can start a statement.
extern const TokenCode tlStatementStart[] = {
    TokenCode::BEGIN, TokenCode::CASE, TokenCode::FOR, TokenCode::IF, TokenCode::REPEAT, TokenCode::WHILE, TokenCode::Identifier,
    TokenCode::Dummy
};

//--Tokens that can follow a statement.
extern const TokenCode tlStatementFollow[] = {
    TokenCode::Semicolon, TokenCode::Period, TokenCode::END, TokenCode::ELSE, TokenCode::UNTIL, TokenCode::Dummy
};

//--Tokens that can start a CASE label.
extern const TokenCode tlCaseLabelStart[] = {
    TokenCode::Identifier, TokenCode::Number, TokenCode::Plus, TokenCode::Minus, TokenCode::String, TokenCode::Dummy
};

//--Tokens that can start an expression.
extern const TokenCode tlExpressionStart[] = {
    TokenCode::Plus, TokenCode::Minus, TokenCode::Identifier, TokenCode::Number, TokenCode::String,
    TokenCode::NOT, TokenCode::LParen, TokenCode::Dummy
};

//--Tokens that can follow an expression.
extern const TokenCode tlExpressionFollow[] = {
    TokenCode::Comma, TokenCode::RParen, TokenCode::RBracket, TokenCode::Colon, TokenCode::THEN, TokenCode::TO, TokenCode::DOWNTO,
    TokenCode::DO, TokenCode::OF, TokenCode::Dummy
};

//--Tokens that can start a subscript or field.
extern const TokenCode tlSubscriptOrFieldStart[] = {
    TokenCode::LBracket, TokenCode::Period, TokenCode::Dummy
};

//--Relational operators.
extern const TokenCode tlRelOps[] = {
    TokenCode::Equal, TokenCode::Ne, TokenCode::Lt, TokenCode::Gt, TokenCode::Le, TokenCode::Ge, TokenCode::Dummy
};

//--Unary + and - operators.
extern const TokenCode tlUnaryOps[] = {
    TokenCode::Plus, TokenCode::Minus, TokenCode::Dummy
};

//--Additive operators.
extern const TokenCode tlAddOps[] = {
    TokenCode::Plus, TokenCode::Minus, TokenCode::OR, TokenCode::Dummy
};

//--Multiplicative operators.
extern const TokenCode tlMulOps[] = {
    TokenCode::Star, TokenCode::Slash, TokenCode::DIV, TokenCode::MOD, TokenCode::AND, TokenCode::Dummy
};

//--Tokens that can end a program.
extern const TokenCode tlProgramEnd[] = {
    TokenCode::Period, TokenCode::Dummy
};

//--Individual tokens.
extern const TokenCode tlColonEqual[] = {TokenCode::ColonEqual,   TokenCode::Dummy};
extern const TokenCode tlDO[]         = {TokenCode::DO,           TokenCode::Dummy};
extern const TokenCode tlTHEN[]       = {TokenCode::THEN,         TokenCode::Dummy};
extern const TokenCode tlTODOWNTO[]   = {TokenCode::TO, TokenCode::DOWNTO, TokenCode::Dummy};
extern const TokenCode tlOF[]         = {TokenCode::OF,           TokenCode::Dummy};
extern const TokenCode tlColon[]      = {TokenCode::Colon,        TokenCode::Dummy};
extern const TokenCode tlEND[]        = {TokenCode::END,          TokenCode::Dummy};

//--------------------------------------------------------------
//  Check if a token code is in the token list.
//
//      tc    : token code
//      pList : ptr to TokenCode::Dummy-terminated token list
//
//  Return:  true if in list, false if not or empty list
//--------------------------------------------------------------
int TokenIn(TokenCode tc, const TokenCode *pList)
{
    const TokenCode *pCode;   // ptr to token code in list

    if (!pList) return false;  // empty list

    for (pCode = pList; (bool)*pCode; ++pCode) // TODO: I am explicitly casting *pCode to bool, and this is a hack
    {
        if (*pCode == tc) return true;  // in list
    }

    return false;  // not in list
}
