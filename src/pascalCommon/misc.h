//  *************************************************************
//  *                                                           *
//  *   M I S C E L L A N E O U S   (Header)            *
//  *                                                           *
//  *   FILE:    prog13-1/misc.h                *
//  *                                                           *
//  *   MODULE:  Common                                         *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#ifndef misc_h
#define misc_h

/*
const int false = 0;
const int true  = 1;
*/

//--------------------------------------------------------------
//  Runtime stack frame offsets
//--------------------------------------------------------------

const int procLocalsStackFrameOffset =  0;
const int funcLocalsStackFrameOffset = -4;
const int parametersStackFrameOffset = +6;

//--------------------------------------------------------------
//  CharacterCode           Character codes.
//--------------------------------------------------------------
enum class CharacterCode 
{
    ccLetter, ccDigit, ccSpecial, ccQuote, ccWhiteSpace,
    ccEndOfFile, ccError
};

//--------------------------------------------------------------
//  TokenCode          Token codes.
//--------------------------------------------------------------
enum class TokenCode 
{
    Dummy,
    Identifier, Number, String, EndOfFile, Error,

    UpArrow, Star, LParen, RParen, Minus, Plus,
    Equal, LBracket, RBracket, Colon, Semicolon, Lt,
    Gt, Comma, Period, Slash, ColonEqual, Le, Ge,
    Ne, DotDot,
    
    AND, ARRAY, BEGIN, CASE, CONST, DIV,
    DO, DOWNTO, ELSE, END, FILE, FOR, FUNCTION,
    GOTO, IF, IN, LABEL, MOD, NIL, NOT, OF, OR,
    PACKED, PROCEDURE, PROGRAM, RECORD, REPEAT, SET,
    THEN, TO, TYPE, UNTIL, VAR, WHILE, WITH,
};

//--------------------------------------------------------------
//  DataType           Data type.
//--------------------------------------------------------------
enum class DataType 
{
    Dummy, Integer, Real, Character, String,
};

//--------------------------------------------------------------
//  TDataValue          Data value.
//--------------------------------------------------------------
union TDataValue {
    int    integer;
    float  real;
    char   character;
    char  *pString;     //TODO: Update to C++ string pointer?
};  

#endif
