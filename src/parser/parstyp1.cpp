//fig 7-13
//  *************************************************************
//  *                                                           *
//  *   P A R S E R   (Types #1)                                *
//  *                                                           *
//  *   Parse type definitions, and identifier, enumeration,    *
//  *   and subrange type specifications.                       *
//  *                                                           *
//  *   CLASSES: TParser                                        *
//  *                                                           *
//  *   FILE:    prog7-1/parstyp1.cpp                           *
//  *                                                           *
//  *   MODULE:  Parser                                         *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include "commonpascal.h"
#include "parser.h"

//--------------------------------------------------------------
//  Parse a list of type definitions separated by semicolons:
//
//                              <id> = <type>
//
//      pRoutineId : ptr to symbol table node of program,
//                   procedure, or function identifier
//--------------------------------------------------------------
void TParser::ParseTypeDefinitions(TSymtabNode *pRoutineId)
{
    TSymtabNode *pLastId = NULL;  // ptr to last type id node
                  //   in local list

    //--Loop to parse a list of type definitions
    //--seperated by semicolons.
    while (token == TokenCode::Identifier) {

    //--<id>
    TSymtabNode *pTypeId = EnterNewLocal(pToken->String());

    //--Link the routine's local type id nodes together.
    if (!pRoutineId->defn.routine.locals.pTypeIds) {
        pRoutineId->defn.routine.locals.pTypeIds = pTypeId;
    }
    else {
        pLastId->next = pTypeId;
    }
    pLastId = pTypeId;

    //-- =
    GetToken();
    CondGetToken(TokenCode::Equal, errMissingEqual);

    //--<type>
    SetType(pTypeId->pType, ParseTypeSpec());
    pTypeId->defn.how = dcType;

    //--If the type object doesn't have a name yet,
    //--point it to the type id.
    if (! pTypeId->pType->pTypeId) {
        pTypeId->pType->pTypeId = pTypeId;
    }

    //-- ;
    Resync(tlDeclarationFollow, tlDeclarationStart,
           tlStatementStart);
    CondGetToken(TokenCode::Semicolon, errMissingSemicolon);

    //--Skip extra semicolons.
    while (token == TokenCode::Semicolon) GetToken();
    Resync(tlDeclarationFollow, tlDeclarationStart,
           tlStatementStart);
    }
}

//--------------------------------------------------------------
//  Parse a type specification.
//
//  Return: ptr to type object
//--------------------------------------------------------------
TType *TParser::ParseTypeSpec(void)
{
    switch (token) {

    //--Type identifier
    case TokenCode::Identifier: {
        TSymtabNode *pId = Find(pToken->String());

        switch (pId->defn.how) {
        case dcType:      return ParseIdentifierType(pId);
        case dcConstant:  return ParseSubrangeType(pId);

        default:
            Error(errNotATypeIdentifier);
            GetToken();
            return(pDummyType);
        }
    }

    case TokenCode::LParen:  return ParseEnumerationType();
    case TokenCode::ARRAY:   return ParseArrayType();
    case TokenCode::RECORD:  return ParseRecordType();

    case TokenCode::Plus:
    case TokenCode::Minus:
    case TokenCode::Number:
    case TokenCode::String:  return ParseSubrangeType(NULL);

    default:
        Error(errInvalidType);
        return(pDummyType);
    }
}


//              *********************
//              *                   *
//              *  Identifier Type  *
//              *                   *
//              *********************

//--------------------------------------------------------------
//  In a type defintion of the form
//
//                               <id-1> = <id-2>
//
//                          parse <id-2>.
//
//      pId2 : ptr to symbol table node of <id-2>
//
//  Return: ptr to type object of <id-2>
//--------------------------------------------------------------
TType *TParser::ParseIdentifierType(const TSymtabNode *pId2)
{
    GetToken();
    return pId2->pType;
}


//              **********************
//              *                    *
//              *  Enumeration Type  *
//              *                    *
//              **********************

//--------------------------------------------------------------
//  Parse a enumeration type specification:
//
//                               ( <id-1>, <id-2>, ..., <id-n> )
//
//  Return: ptr to type object
//--------------------------------------------------------------
TType *TParser::ParseEnumerationType(void)
{
    TType       *pType      = new TType(fcEnum, sizeof(int), NULL);
    TSymtabNode *pLastId    = NULL;
    int          constValue = -1;  // enumeration constant value

    GetToken();
    Resync(tlEnumConstStart);

    //--Loop to parse list of constant identifiers separated by commas.
    while (token == TokenCode::Identifier) {
    TSymtabNode *pConstId = EnterNewLocal(pToken->String());
    ++constValue;

    if (pConstId->defn.how == dcUndefined) {
        pConstId->defn.how = dcConstant;
        pConstId->defn.constant.value.integer = constValue;
        SetType(pConstId->pType, pType);

        //--Link constant identifier symbol table nodes together.
        if (!pLastId) {
        pType->enumeration.pConstIds = pLastId = pConstId;
        }
        else {
        pLastId->next = pConstId;
        pLastId       = pConstId;
        }
    }

    //-- ,
    GetToken();
    Resync(tlEnumConstFollow);
    if (token == TokenCode::Comma) {

        //--Saw comma.  Skip extra commas and look for
        //--            an identifier.
        do {
        GetToken();
        Resync(tlEnumConstStart, tlEnumConstFollow);
        if (token == TokenCode::Comma) Error(errMissingIdentifier);
        } while (token == TokenCode::Comma);
        if (token != TokenCode::Identifier) Error(errMissingIdentifier);
    }
    else if (token == TokenCode::Identifier) Error(errMissingComma);
    }

    //-- )
    CondGetToken(TokenCode::RParen, errMissingRightParen);

    pType->enumeration.max = constValue;
    return pType;
}


//              *******************
//              *                 *
//              *  Subrange Type  *
//              *                 *
//              *******************

//--------------------------------------------------------------
//  Parse a subrange type specification:
//
//                               <min-const> .. <max-const>
//
//      pMinId : ptr to symbol table node of <min-const> if it
//               is an identifier, else NULL
//
//  Return: ptr to type object
//--------------------------------------------------------------
TType *TParser::ParseSubrangeType(TSymtabNode *pMinId)
{
    TType *pType = new TType(fcSubrange, 0, NULL);

    //--<min-const>
    SetType(pType->subrange.pBaseType,
        ParseSubrangeLimit(pMinId, pType->subrange.min));

    //-- ..
    Resync(tlSubrangeLimitFollow, tlDeclarationStart);
    CondGetToken(TokenCode::DotDot, errMissingDotDot);

    //--<max-const>
    TType *pMaxType = ParseSubrangeLimit(NULL, pType->subrange.max);

    //--Check limits.
    if (pMaxType != pType->subrange.pBaseType) {
    Error(errIncompatibleTypes);
    pType->subrange.min = pType->subrange.max = 0;
    }
    else if (pType->subrange.min > pType->subrange.max) {
    Error(errMinGtMax);

    int temp = pType->subrange.min;
    pType->subrange.min = pType->subrange.max;
    pType->subrange.max = temp;
    }

    pType->size = pType->subrange.pBaseType->size;
    return pType;
}

//--------------------------------------------------------------
//  Parse a mininum or maximum limit constant of a subrange type.
//
//      pLimitId : ptr to symbol table node of limit constant if
//                 it is an identifier (already set for the
//                 minimum limit), else NULL
//      limit    : ref to limit value that will be set
//
//  Return: ptr to type object of limit constant
//--------------------------------------------------------------
TType *TParser::ParseSubrangeLimit(TSymtabNode *pLimitId, int &limit)
{
    TType      *pType = pDummyType;  // type to return
    TokenCode  sign  = TokenCode::Dummy;     // unary + or - sign, or none

    limit = 0;

    //--Unary + or -
    if (TokenIn(token, tlUnaryOps)) {
    if (token == TokenCode::Minus) sign = TokenCode::Minus;
    GetToken();
    }

    switch (token) {

    case TokenCode::Number:

        //--Numeric constant:  Integer type only.
        if (pToken->Type() == DataType::Integer) {
        limit = sign == TokenCode::Minus ? -pToken->Value().integer
                    :  pToken->Value().integer;
        pType = pIntegerType;
        }
        else Error(errInvalidSubrangeType);
        break;

    case TokenCode::Identifier:

        //--Identifier limit:  Must be integer, character, or
        //--                   enumeration type.
        if (!pLimitId) pLimitId = Find(pToken->String());

        if (pLimitId->defn.how == dcUndefined) {
        pLimitId->defn.how = dcConstant;
        pType = SetType(pLimitId->pType, pDummyType);
        break;
        }

        else if ((pLimitId->pType == pRealType)  ||
             (pLimitId->pType == pDummyType) ||
             (pLimitId->pType->form == fcArray)) {
        Error(errInvalidSubrangeType);
        }
        else if (pLimitId->defn.how == dcConstant) {

        //--Use the value of the constant identifier.
        if (pLimitId->pType == pIntegerType) {
            limit = sign == TokenCode::Minus
            ? -pLimitId->defn.constant.value.integer
            :  pLimitId->defn.constant.value.integer;
        }
        else if (pLimitId->pType == pCharType) {
            if (sign != TokenCode::Dummy) Error(errInvalidConstant);
            limit = pLimitId->defn.constant.value.character;
        }
        else if (pLimitId->pType->form == fcEnum) {
            if (sign != TokenCode::Dummy) Error(errInvalidConstant);
            limit = pLimitId->defn.constant.value.integer;
        }
        pType = pLimitId->pType;
        }

        else Error(errNotAConstantIdentifier);
        break;

    case TokenCode::String:

        //--String limit:  Character type only.
        if (sign != TokenCode::Dummy) Error(errInvalidConstant);

        if (strlen(pToken->String()) != 3) { // len includes quotes
        Error(errInvalidSubrangeType);
        }

        limit = pToken->String()[1];
        pType = pCharType;
        break;

    default:
        Error(errMissingConstant);
        return pType;  // don't get another token
    }

    GetToken();
    return pType;
}

