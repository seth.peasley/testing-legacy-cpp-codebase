//fig 3-19
//  *************************************************************
//  *                                                           *
//  *   T O K E N S   (Strings and Specials)                    *
//  *                                                           *
//  *   Routines to extract string and special symbol tokens    *
//  *   from the source file.                                   *
//  *                                                           *
//  *   CLASSES: TStringToken, TSpecialToken, TErrorToken       *
//  *                                                           *
//  *   FILE:    prog3-2/tknstrsp.cpp                           *
//  *                                                           *
//  *   MODULE:  Scanner                                        *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include <stdio.h>
#include "token.h"


//              *******************
//              *                 *
//              *  String Tokens  *
//              *                 *
//              *******************

//--------------------------------------------------------------
//  Get a string token from the source.
//
//      pBuffer : ptr to text input buffer
//--------------------------------------------------------------
void TStringToken::Get(TTextInBuffer &buffer)
{
    char  ch;           // current character
    char *ps = string;  // ptr to char in string

    *ps++ = '\'';  // opening quote

    //--Get the string.
    ch = buffer.GetChar();  // first char after opening quote
    while (ch != eofChar) {
    if (ch == '\'') {     // look for another quote

        //--Fetched a quote.  Now check for an adjacent quote,
        //--since two consecutive quotes represent a single
        //--quote in the string.
        ch = buffer.GetChar();
        if (ch != '\'') break;  // not another quote, so previous
                    //   quote ended the string
    }

    //--Replace the end of line character with a blank.
    else if (ch == '\0') ch = ' ';

    //--Append current char to string, then get the next char.
    *ps++ = ch;
    ch = buffer.GetChar();
    }

    if (ch == eofChar) Error(errUnexpectedEndOfFile);

    *ps++ = '\'';  // closing quote
    *ps   = '\0';
}

//--------------------------------------------------------------
//  Print the token to the list file.
//--------------------------------------------------------------
void TStringToken::Print(void) const
{
    sprintf(list.text, "\t%-18s %-s", ">> string:", string);
    list.PutLine();
}


//              ********************
//              *                  *
//              *  Special Tokens  *
//              *                  *
//              ********************

//--------------------------------------------------------------
//  Extract a one- or two-character special symbol token from the source.
//
//      pBuffer : ptr to text input buffer
//--------------------------------------------------------------
void TSpecialToken::Get(TTextInBuffer &buffer)
{
    char  ch = buffer.Char();
    char *ps = string;

    *ps++ = ch;

    switch (ch) {
    case '^':   code = TokenCode::UpArrow;    buffer.GetChar();  break;
    case '*':   code = TokenCode::Star;       buffer.GetChar();  break;
    case '(':   code = TokenCode::LParen;     buffer.GetChar();  break;
    case ')':   code = TokenCode::RParen;     buffer.GetChar();  break;
    case '-':   code = TokenCode::Minus;      buffer.GetChar();  break;
    case '+':   code = TokenCode::Plus;       buffer.GetChar();  break;
    case '=':   code = TokenCode::Equal;      buffer.GetChar();  break;
    case '[':   code = TokenCode::LBracket;   buffer.GetChar();  break;
    case ']':   code = TokenCode::RBracket;   buffer.GetChar();  break;
    case ';':   code = TokenCode::Semicolon;  buffer.GetChar();  break;
    case ',':   code = TokenCode::Comma;      buffer.GetChar();  break;
    case '/':   code = TokenCode::Slash;      buffer.GetChar();  break;

    case ':':   ch = buffer.GetChar();     // : or :=
            if (ch == '=') {
            *ps++ = '=';
            code  = TokenCode::ColonEqual;
            buffer.GetChar();
            }
            else code = TokenCode::Colon;
            break;

    case '<':   ch = buffer.GetChar();     // < or <= or <>
            if (ch == '=') {
            *ps++ = '=';
            code  = TokenCode::Le;
            buffer.GetChar();
            }
            else if (ch == '>') {
            *ps++ = '>';
            code  = TokenCode::Ne;
            buffer.GetChar();
            }
            else code = TokenCode::Lt;
            break;

    case '>':   ch = buffer.GetChar();     // > or >=
            if (ch == '=') {
            *ps++ = '=';
            code  = TokenCode::Ge;
            buffer.GetChar();
            }
            else code = TokenCode::Gt;
            break;

    case '.':   ch = buffer.GetChar();     // . or ..
            if (ch == '.') {
            *ps++ = '.';
            code  = TokenCode::DotDot;
            buffer.GetChar();
            }
            else code = TokenCode::Period;
            break;

    default:    code = TokenCode::Error;                  // error
            buffer.GetChar();
            Error(errUnrecognizable);
            break;
    }

    *ps = '\0';
}

//--------------------------------------------------------------
//  Print the token to the list file.
//--------------------------------------------------------------
void TSpecialToken::Print(void) const
{
    sprintf(list.text, "\t%-18s %-s", ">> special:", string);
    list.PutLine();
}


//              *****************
//              *               *
//              *  Error Token  *
//              *               *
//              *****************

//--------------------------------------------------------------
//  Extract an invalid character from the source.
//
//      pBuffer : ptr to text input buffer
//--------------------------------------------------------------
void TErrorToken::Get(TTextInBuffer &buffer)
{
    string[0] = buffer.Char();
    string[1] = '\0';

    buffer.GetChar();
    Error(errUnrecognizable);
}
