//fig 3-17
//  *************************************************************
//  *                                                           *
//  *   T O K E N S   (Words)                                   *
//  *                                                           *
//  *   Extract word tokens from the source file.               *
//  *                                                           *
//  *   CLASSES: TWordToken                                     *
//  *                                                           *
//  *   FILE:    prog3-2/tknword.cpp                            *
//  *                                                           *
//  *   MODULE:  Scanner                                        *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *************************************************************

#include <string.h>
#include <stdio.h>
#include "token.h"


//              *************************
//              *                       *
//              *  Reserved Word Table  *
//              *                       *
//              *************************

const int minResWordLen = 2;    // min and max reserved
const int maxResWordLen = 9;    //   word lengths

//--------------------------------------------------------------
//  Reserved word lists (according to word length)
//--------------------------------------------------------------
struct TResWord {
    const char       *pString;  // ptr to word string
    TokenCode  code;     // word code
};

static TResWord rw2[] = {
    {"do", TokenCode::DO}, {"if", TokenCode::IF}, {"in", TokenCode::IN}, {"of", TokenCode::OF},
    {"or", TokenCode::OR}, {"to", TokenCode::TO}, {NULL},
};

static TResWord rw3[] = {
    {"and", TokenCode::AND}, {"div", TokenCode::DIV}, {"end", TokenCode::END}, {"for", TokenCode::FOR},
    {"mod", TokenCode::MOD}, {"nil", TokenCode::NIL}, {"not", TokenCode::NOT}, {"set", TokenCode::SET},
    {"var", TokenCode::VAR}, {NULL},
};

static TResWord rw4[] = {
    {"case", TokenCode::CASE}, {"else", TokenCode::ELSE}, {"file", TokenCode::FILE},
    {"goto", TokenCode::GOTO}, {"then", TokenCode::THEN}, {"type", TokenCode::TYPE},
    {"with", TokenCode::WITH}, {NULL},
};

static TResWord rw5[] = {
    {"array", TokenCode::ARRAY}, {"begin", TokenCode::BEGIN}, {"const", TokenCode::CONST},
    {"label", TokenCode::LABEL}, {"until", TokenCode::UNTIL}, {"while", TokenCode::WHILE},
    {NULL},
};

static TResWord rw6[] = {
    {"downto", TokenCode::DOWNTO}, {"packed", TokenCode::PACKED}, {"record", TokenCode::RECORD},
    {"repeat", TokenCode::REPEAT}, {NULL},
};

static TResWord rw7[] = {
    {"program", TokenCode::PROGRAM}, {NULL},
};

static TResWord rw8[] = {
    {"function", TokenCode::FUNCTION}, {NULL},
};

static TResWord rw9[] = {
    {"procedure", TokenCode::PROCEDURE}, {NULL},
};

//--------------------------------------------------------------
//  The reserved word table
//--------------------------------------------------------------
static TResWord *rwTable[] = {
    NULL, NULL, rw2, rw3, rw4, rw5, rw6, rw7, rw8, rw9,
};


//              *****************
//              *               *
//              *  Word Tokens  *
//              *               *
//              *****************


// The method strlwr is a non-standard C++ call, leftover in this code from early MS compilers. 
// This version is taken from here: https://stackoverflow.com/a/23618467
char *strlwr(char *str)
{
  unsigned char *p = (unsigned char *)str;

  while (*p) {
     *p = tolower((unsigned char)*p);
      p++;
  }

  return str;
}


//--------------------------------------------------------------
//  Extract a word token from the source and downshift its characters.  
//  Check if it's a reserved word.
//
//      pBuffer : ptr to text input buffer
//--------------------------------------------------------------
void TWordToken::Get(TTextInBuffer &buffer)
{
    extern CharacterCode charCodeMap[];

    char  ch = buffer.Char();  // char fetched from input
    char *ps = string;

    //--Get the word.
    do {
    *ps++ = ch;
    ch = buffer.GetChar();
    } while (   (charCodeMap[ch] == CharacterCode::ccLetter)
         || (charCodeMap[ch] == CharacterCode::ccDigit));

    *ps = '\0';
    strlwr(string);        // downshift its characters

    CheckForReservedWord();
}

//--------------------------------------------------------------
//  Is the word token a reserved word? If yes, set the its token code to
//  the appropriate code.  If not, set the token code to TokenCode::Identifier.
//--------------------------------------------------------------
void TWordToken::CheckForReservedWord(void)
{
    int       len = strlen(string);
    TResWord *prw;        // ptr to elmt of reserved word table

    code = TokenCode::Identifier;  // first assume it's an identifier

    //--Is it the right length?
    if ((len >= minResWordLen) && (len <= maxResWordLen)) {

    //--Yes.  Use the word length to pick the appropriate list
    //--from the reserved word table and check to see if the word
    //--is in there.
    for (prw = rwTable[len]; prw->pString; ++prw) {
        if (strcmp(string, prw->pString) == 0) {
        code = prw->code;  // yes: set reserved word token code
        break;
        }
    }
    }
}

//--------------------------------------------------------------
//  Print the token to the list file.
//--------------------------------------------------------------
void TWordToken::Print(void) const
{
    if (code == TokenCode::Identifier) {
    sprintf(list.text, "\t%-18s %-s", ">> identifier:", string);
    }
    else {
    sprintf(list.text, "\t%-18s %-s", ">> reserved word:", string);
    }

    list.PutLine();
}
