	DOSSEG
	.MODEL  small
	.STACK  1024

	.CODE

	PUBLIC	_PascalMain
	INCLUDE	pasextrn.inc

$STATIC_LINK		EQU	<WORD PTR [bp+4]>
$RETURN_VALUE		EQU	<WORD PTR [bp-4]>
$HIGH_RETURN_VALUE	EQU	<WORD PTR [bp-2]>

; PROGRAM hello (output)

; VAR
; i : integer

_PascalMain	PROC

	push	bp
	mov	bp,sp
; BEGIN
; {9} FOR i := 1 TO 10 DO
	mov	ax,1
	mov	WORD PTR i_030,ax
$L_034:
	mov	ax,10
	cmp	WORD PTR i_030,ax
	jle	$L_035
	jmp	$L_036
$L_035:
; BEGIN
; {10} writeln('Hello, world.')
	lea	ax,WORD PTR $S_033
	push	ax
	mov	ax,0
	push	ax
	mov	ax,13
	push	ax
	call	_WriteString
	add	sp,6
	call	_WriteLine
; END
	inc	WORD PTR i_030
	jmp	$L_034
$L_036:
	dec	WORD PTR i_030
; END

	pop	bp
	ret

_PascalMain	ENDP

	.DATA

i_030	DW	0
$S_033	DB	"Hello, world."

	END
