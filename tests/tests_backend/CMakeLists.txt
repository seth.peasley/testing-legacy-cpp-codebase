# # # # this CMAKE file based on guidance from here:
# https://crascit.com/2016/01/31/enhanced-source-file-handling-with-target_sources/



target_sources(UnitTests
    PRIVATE
        tests_backend.cpp
        tests_backend.h
)

target_include_directories(UnitTests 
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}
        #These includes feel like hacks. I'm not especially fond of this, I feel like there is a more elegant way. But this works, and I've
        # spent three days on this. This is definitely someting to revist in the future. 
        ${PROJECT_SOURCE_DIR}/src/backend
        ${PROJECT_SOURCE_DIR}/src/buffer
        ${PROJECT_SOURCE_DIR}/src/codeGenerator
        ${PROJECT_SOURCE_DIR}/src/errors
        ${PROJECT_SOURCE_DIR}/src/intermediateCode
        ${PROJECT_SOURCE_DIR}/src/parser
        ${PROJECT_SOURCE_DIR}/src/pascalCommon
        ${PROJECT_SOURCE_DIR}/src/scanner
        ${PROJECT_SOURCE_DIR}/src/symbolTable
        ${PROJECT_SOURCE_DIR}/src/tokens
        ${PROJECT_SOURCE_DIR}/src/types
        ${Catch2_SOURCE_DIR}/single_include/catch2 # I do not understand why adding this reference works, but it does.
)

