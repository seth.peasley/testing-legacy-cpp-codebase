#include "catch.hpp"
#include <iostream>

#include "parser.h"

//TType someType = new TType(7);

unsigned int Factorial( unsigned int number ) {
    //std::cout << "factori" << std::endl;
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "Factorials are computed", "[factorial]" ) 
{
    std::cout << "here" << std::endl;
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}
