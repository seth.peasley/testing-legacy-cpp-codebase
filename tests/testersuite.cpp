//#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
//#include "tests_backend.h"

//  #include "test_parser_parse_declarations.cpp"
//  #include "tests_backend.cpp"
#include <iostream>


int main(void)
{ 
    int result = Catch::Session().run();
    std::cout << "stuff should happen" << std::endl;
    return result;
}

TEST_CASE("NOP TEst case", "[meh]") {}