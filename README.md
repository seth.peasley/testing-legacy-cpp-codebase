A project to take some pre-ISO standard C++ code and update a bit, and also add unit tests.

The code is from Ronald Mak's _Writing Compilers and Interpreters: An Applied Approach Using C++_, published in 1996 by John Wiley & Sons.
